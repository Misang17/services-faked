var express = require('express');
var router = express.Router();
var moment = require('moment');
var fs = require("fs");
var path = require('path');


var centerClinic = require('../../mock/centerClinic/centros.json');
var centerClinic1 = require('../../mock/centerClinic/centros1.json');
var centerClinic2 = require('../../mock/centerClinic/centros2.json');
var centerClinic3 = require('../../mock/centerClinic/centros3.json');
var centerClinic4 = require('../../mock/centerClinic/centros4.json');
var centerClinic5 = require('../../mock/centerClinic/centros5.json');

//Puebas
var pruebasEstado = require('../../mock/centerClinic/pruebasEstado.json');
var pruebasTotales = require('../../mock/centerClinic/pruebasTotales.json');


/* GET users listing. */
router.use(function(req, res, next) {
    var host = req.get('origin');
    res.setHeader('Access-Control-Allow-Origin', host||"*" || 'https://covidmx.byglob.com');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,tsec');
    res.setHeader('Access-Control-Request-Headers', 'X-Requested-With,content-type,tsec');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

router.get('/', function(req, res, next) {
  res.render('clinic', { title: '' });
});


router.get('/getClinics', function(req, res, next) {
  var states = req.query.state;
  if(states == "" || states == "null" || states == "undefined"){
    return res.json(centerClinic);
  }else {
    var num = Math.floor (Math.random () * 5);
    switch (num) {
      case 1:
        console.log("Trajo el 1")
        return res.json(centerClinic1);
        case 2:
          console.log("Trajo el 2")
          return res.json(centerClinic2);
          case 3:
            console.log("Trajo el 3")
            return res.json(centerClinic3);
            case 4:
              console.log("Trajo el 4")
              return res.json(centerClinic4);
              case 5:
                console.log("Trajo el 5")
                return res.json(centerClinic5);
    }
  }
  next();
  });

  router.get('/getPruebas', function(req, res, next) {
    var totales = req.query.total;
    var estado = req.query.state;
    if(totales == "" || totales == "null" || totales == "undefined"){
      return res.json(pruebasEstado);
    }else if (totales == "false" || totales == false){
      return res.json(pruebasTotales);
    }
    next();
    });

module.exports = router;
